class CreateUsersTeams < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :description
      t.timestamps null: false
    end

    create_table :teams do |t|
      t.string :name
      t.timestamps null: false
    end

    create_table :teams_users do |t|
      t.belongs_to :user, index: true
      t.belongs_to :team, index: true
    end
  end
end
